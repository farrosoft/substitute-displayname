<?php
/*
Plugin Name: Substitute Displayname
Plugin URI: http://farrosoft.com/
Version: 1.1.0
Description: Substitutes a default displayname for new users.
Author: Farrosoft
Author URI:  http://farrosoft.com/
*/

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

require_once(__DIR__ . '/includes/functions.php');

/* Version check */
global $wp_version;
$exit_msg='Substitute Displayname requires WordPress 2.5 or newer.  <a href="http://codex.wordpress.org/Upgrading_WordPress">Please update!</a>';
if (version_compare($wp_version,"2.5","<")) {
    exit ($exit_msg);
}